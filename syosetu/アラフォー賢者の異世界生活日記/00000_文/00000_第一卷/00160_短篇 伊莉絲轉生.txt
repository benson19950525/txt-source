「入江澄香」，十四歲。

是個在附近市立國中上學，極為普通的國中生。

父親是貿易公司的中階管理職，母親在附近的小菜店打工，弟弟則是國中棒球隊的王牌。她就是在這種真的極為普通的家庭裡長大。

她幾乎沒有社交關係，基本上和周圍的人講話都不對盤。她也有自覺自己有點脫離一般社會常識。畢竟，她對藝人或流行這種所謂的女孩話題都不感興趣，反而有些回避的傾向。

她找不到與年紀相仿的少女們的共同之處，主要喜歡遊戲故事或角色、以前的漫畫或搞笑藝人這些東西，當然經常和其他女生們聊不起來，所以就變得很自卑了。

就結果來說，她被孤立也是遲早的問題。她被周遭認為會「異於他人地冷眼旁觀事物」，是會被附近的人說「陰沉」、「很宅」之類的孩子。

雖然並不是對家庭有所不滿，但不知為何總是窩在房裡，因此就變成了存在感薄弱的人。不如說，弟弟比較受到疼愛，但這些對澄香而言都是無所謂的事。

她的興趣是輕小說和遊戲。半年前拿出大筆壓歲錢買下了網路遊戲「Sword and Sorcery Ⅶ」並且沉迷其中。那天，她也在遊戲裡試圖精通魔法職業，不斷反覆進行升等行為。

沒錯，是截至那天為止……

「……這裡是哪裡？」

回過神來，所在之處便是一片草原。

周圍還很明亮，可以看見太陽掛在天空的正上方，讓她了解到現在應該是過了正午的時分。

然而，她覺得難以置信的，應該就是有兩輪月亮的這件事吧。她不由得揉揉眼皮，試著確認好幾次自己的眼睛是不是有問題，但怎麼看，天上依然是兩個月亮。

「這……難不成是異世界召喚！不會吧，真的假的！是劍與魔法的世界？或許我會被叫做勇者，太好啦────！是冒險耶！」

我們通常會在此感到困惑，準備好情勢判斷之前會很慌張，但在那裡的人目前是國中生。

就像有中二病這個詞一樣，她好像有期盼非現實狀況的傾向，很輕易就理解了自己的狀況，接著開始摸索下一步要做的事。

當然，因為她在空無一物的草原上，以城鎮為目標，就會是約定俗成的發展。

不過，她有事情必須在那之前做。

「如果是慣例的話，或許可以看見能力參數吧？能力參數，開啟──！」

澄香因為那是異世界慣例之物而展開了行動。喊得格外用力。

不過，能力參數畫面就如她想像的那樣打了開來。她對這狀況感到很興奮。但……

「郵件？會是誰呢……啊，難不成是神明？或召喚我的某個人？」

從郵件標題是『關於你現在發生的事』看來，她覺得那是說明自己為何會來到這個世界的信件。

澄香滿心雀躍地打開那封信。

『我是風之女神溫蒂雅。說明啊～因為很麻煩，我就說明重點喲。』

內容好像相當隨興。

『過去啊～勇者打敗了邪神～因為沒徹底殺掉，所以就只好封印祂了呢～

然後啊～因為封印衰弱，我們很煩惱該怎麼辦，最後就抽簽決定再次封印到異世界裡了呢。沒錯，就是指你們在玩的那個世界呢。你們能打敗那位邪神是很好～但通常根本就不會想到祂居然會捲入在那個世界玩遊戲的人並自爆吧？

因為我們是瞞著那邊的諸神進行，所以祂們非常不滿呢～沒辦法，所以就讓你轉生到這邊的世界了。嗯，要死要活都請便。

光是讓你重生就算很親切了吧？順帶一提，你在遊戲時的持有物，我全都幫你在這邊世界重新構築了，你要覺得慶幸。就這樣～』

這真的很隨便，極為不負責任。那個女神什麼的完全放棄了管理責任。而且給其他世界的諸神添了麻煩，也看不出任何反省之色。

澄香可以理解的，就只有自己已經死了的這點。

「這不是異世界召喚，而是異世界轉生嗎……我再也見不到媽媽他們了……」

想也不用想，內容很殘酷。

他們徑自把邪神封印到不屬於他人世界，還是其他神明在管理的世界，同時，儘管結果演變成奪走許多人命，字面上卻完全看不見反省的樣子。

何止如此，還可以讀出有種將錯就錯的感覺。祂們的行動太隨便了。

完全沒有那種像是勇者，或為防世界滅亡等等的背景。那純粹是人為事故與邪神自爆恐攻，澄香受到了不少打擊。

總之，她是沒任何理由就被殺，只是別人為了逃避那項責任，才讓自己轉生到這個世界。關於那件事，這個世界的諸神完全沒在反省。

反倒說了『我讓你轉生到這邊的世界了，你就覺得慶幸吧。之後就隨意地活下去吧。』

叫人就這樣接受有點不合理。嗯，通常來說是這樣。

不過澄香並不需要什麼理由。

從無聊的日常中解放，倒不如說，她很開心能踏入未知世界。想到接著有冒險生活等著，她就止不住胸口的悸動。

總之，她就是屬於比較輕微的中二病。

「不論如何，既然都來到了異世界，我得完成經典發展呢♪首先是找到城鎮。」

就算待在空無一物的草原上也沒有意義，為了有目的行動，她決定先以城鎮當作目標。

她並不是有什麼目的地，但無論如何「食衣住」都是必要的，不找到據點城鎮，什麼也不會開始。

從澄香是在現代社會上生活過的女孩子這點看來，她當然不可能露宿野外。

於是，她發現了一件事。

「……我沒辦法在野外求生，怎麼辦……」

食衣住當中，「食」是最重要的。室內派的澄香當然沒有野外求生的技能。她回到家就是專心打遊戲，除此之外，邊吃點心邊看電視，就是她每日的例行公事。

就算沒有住宿的地方，休息在哪裡都可以辦到，但問題在於食物。既然她沒有狩獵經驗，要確保糧食就會很困難。澄香的冒險一下子就碰了釘子。

就她邊走邊看見的能力參數，她弄清了現在保持著遊戲時虛擬角色的規格。

即使如此，她的等級是237。就這個世界的基準來說，是位一流的魔導士。應該遠比初出茅廬的傭兵還強吧。然而，她在設定上仍是非常缺乏近戰等能力層面的攻擊型魔導士。

假如「飛龍」或「巴西利斯克」等出現，沒有前衛職業在身邊的澄香，是絕對不可能打倒敵人的。只會有逃命一途。

假如碰見就確定會死亡，難得的嶄新人生，就很可能會在此拉下帷幕。

「嗚……肚子餓了～城鎮在哪裡～？」

真哀傷，她的道具欄裡沒有食材，只有回復道具或素材。

她獨自走著路，心裡只有不安，於是漸漸流下眼淚。

幸好澄香很走運。

「啊……是村子。」

在太陽下山，周圍開始染上黑暗時，澄香在走了大約一小時的山丘前，發現了朴質但感覺是村莊的人造設施。隨著愈來愈靠近，她了解到一件事，就是那好像不算是很大的村莊。

渴望村莊的她跑了起來。

村子四周圍上了以木板釘成的牆壁，澄香也可以理解，這是以防御為目的的障壁。那麼一來，魔物棲息的可能性就會很高。

「有人在沒錯吧？來想辦法請他們分我食物吧！」

澄香重新提起幹勁，重要的防備心一下就拋開了，她全力跑了起來。一般來想，這是很危險的徵兆，但此時的澄香太高興了，彷佛變成一輛失控的特急列車。

她在抵達村莊前面是有發現這點，也有盜賊這種壞人出現的可能性。

輕小說裡也有過整村都連手犯罪的這種事，所以澄香在最後關頭恢復戒心，準備好隨時都能攻擊的架式，一面謹慎地潛入村子的大門。

幸好那是普通的村莊，但她馬上就察覺狀況有點奇怪。

男人們手拿農具四處徘徊，還視地點搭起了路障。

他們環顧四周且戒備著，簡直就像在戒備什麼東西襲擊一樣。

澄香稍微警戒著，同時決定向附近的村人搭話。

「怎麼了嗎？總覺得戒備非常森嚴……」

「嗯？怎麼了，小姑娘。就旅行者來說，你還真是一身輕便呢。你是從哪裡來的？」

澄香的持有物在道具欄裡，所以從身為村人的大叔看來，就旅行來說，想必是覺得她一身輕裝吧。

「我迷了路，想要去城鎮，但不知道該往哪走才好……」

「那還真傷腦筋呢，而且運氣也很差。」

「………………什麼～？」

對澄香來說，光是找得到村落就很值得慶幸了，但她不懂那為什麼會連結到運氣不好。男性農民嘆口氣，親切地為她做說明。

「這座村莊啊，最近一直被哥布林襲擊。村民們還沒出現損害，但未必遲早不會造成損害。最近每天都是這樣。」

「唔哇～……真辛苦呢。」

「很辛苦啊……它們好像是被哥布林的高階種統帥著呢。有統籌行動所以很棘手，還會想把村子裡種的蔬菜連根拔走。城鎮上也來了傭兵，但兩個女人怎麼會有辦法呢？傭兵公會是人手不足嗎？」

她好像來到了很棘手的村莊。

不過，對於連錢都沒有的澄香來說，她也只能受這個村子照顧。

無論如何，她都必須確保能睡覺吃飯的地方。

「那些哥布林們，真是煩得不得了呢。」

「所謂高階種，是指哥布林王？」

「不，好像是『哥布林騎士』。如果是『將軍』的話，這個村子早就被擊潰了。」

對澄香來說，這是一輛順風車。如果是哥布林或哥布林騎士的話，她擁有足以打敗對手的等級。

假如是王的話就會很棘手，但騎士的話，規模就是二十～三十只。靠澄香的魔法擊退是有可能的。她在想，如果在此交涉，似乎可以確保食物或睡一晚的地方。

接著，她決定立刻付諸行動。

「大叔，我也來幫忙吧？別看我這樣，我可是魔導士喲。」

「啊？小姑娘你嗎～？那如果是真的，是很令人感激啦……但是啊，那可不是兒戲喔。」

「我知道喲。相對的，你能分給我住宿地方與食物嗎？我來到這裡之前被可怕的大叔們追，就不小心弄丟了錢和食物呢～」

「如果是魔導士，應該可以反擊對方吧？」

「人數多的話，就算是魔導士也只能逃跑了喲。畢竟被包圍就完蛋了。」

「……我知道了。你應該比之前來的兩位女傭兵還好吧。畢竟女人中的其中一位，看待小孩子們的眼光很不妙。這是為了解決眼前的危機。」

鄉下的男人們很慷慨。

重要的是會把人情放在優先，對有困難的人相當設身處地著想。

更何況是少女獨自旅行，他們在各方面都會很親切。

「成交♪哥布林何時會出現呢？」

「日落時……正好就是現在呢。」

說時遲那時快，瞭望台的警鐘，在壞的那方面上的恰好時間點，高亢地響起了鐘聲。

「是哥布林！哥布林來了～～～～～～！」

「居然來了。你一來馬上就遇到，真是場災難啊。」

「平時都是從哪邊過來的？哥布林的話，應該都會從森林過來。」

就魔物的習性來說，哥布林會棲息在森林或岩山附近，並為求食材而集體行動。

雖然是經典，如果它們的行動有統籌過，領袖級又是高階種的話，群集或戰力變強是很普通的事。包含農田在內，村子的四周都圍著木板釘成的牆壁。就算這樣，它們的體力也比人類優秀，雖然是小嘍囉，也絕對不是能夠小看的魔物。

「是從東側森林來的呢。小姑娘，跟我走！」

「交給我吧，我會把它們全部滅了！」

男人和澄香奔向位於東邊的農田。

因為她也有好幾次團體討伐戰的經驗，所以可以在一定程度上預測魔物的行動。

哥布林的首要目標是食物，其次就是確保繁殖用的雌性。總之，就是人類的女性。哥布林在自然界中是相對弱小的魔物，是被其他魔物捕食的一方。

為了留下子嗣，食物的存在就會很重要，但它們就算狩獵也常反被撲殺。為了繁殖，它們無論如何都有必要確保其他種族的雌性。

就被盯上的這方看來，這應該是場災難吧。

他們一抵達農田，就看到外牆已經被開了個小洞，讓外敵得以入侵。

當哥布林對手的是兩位女性傭兵。身材高挑的女性率性地留著一頭隨風飄逸的長直紅髮，她橫砍一刀就輕鬆擊敗了哥布林。另一位則像在保護她身後似的舉著小圓盾擋開攻擊，並拿著的彎刀費工地砍碎哥布林。

那是留著栗子色鮑伯頭的女性，她眼旁的黑痣很性感。

就澄香所看見的，紅髮女性是E罩杯，栗子色頭髮的女性與其說是C，看來更接近D罩杯。從胸部平坦的澄香看來，那實在讓人很羨慕。

「晃來晃去好美啊～……」

「叔叔，你在看哪裡啊……現在不是緊急狀況嗎？」

「噢，對耶！」

「男人啊……胸部嗎？果然是胸部嗎！女人的魅力難道就是胸部嗎！」

澄香發自內心地吶喊。

澄香的母親只是普通的女性，不過胸部很豐滿。她從小就看著母親的內衣，想說自己有天也會變成那樣。遺憾的是，她的胸部卻是勉強算是平均之下的A罩杯。

由於體型也很嬌小，所以容易被看成比實際年齡還年幼。

以前，她目擊過同年級生看見母親就臉紅的模樣，事後弄清楚了那位同年級生看著的，就是母親的巨乳，並了解到男人全部只對模特兒體型的女性感興趣。

雖然澄香本身也備受疼愛，但是她對此的認知是自己被當小孩對待。在乎體型的澄香覺得這很沒意思。

從青春期少女看來，那種心境在各方面上都很複雜。

就先不說那些事，澄香對哥布林群集接連侵入而來的洞構築了魔法。

「不是比我聽說的還多嗎！去吧，『冰風暴』！」

她製造冰塊射了進去，讓中彈地點一口氣結冰。聚在破洞附近的哥布林於是瞬間凍結，變成品味很差的裝置藝術。

因為是在連發生什麼事都不曉得的期間凍死，在某種意義上，這或許是打倒魔物最溫柔的方式。

「小姑娘，你真厲害耶……」

「洞也堵起來了，但說不定還會被開其他洞。趁現在打倒入侵的哥布林吧。因為數量應該沒那麼多。」

「嗯，交給你啦！這些傢伙的麻煩之處就是數量呢。」

男性農民揮舞柴刀，敲破哥布林的頭。

澄香看見腦漿四濺、衣服染紅的模樣，便摀住了嘴。

那彷佛是殺人瞬間，或是獵奇恐怖片，不是看了會讓人感覺很好的畫面。

那段期間，兩名傭兵也一個接一個擊敗哥布林，再加上村裡的男人們的努力，入侵的哥布林幾乎都被收拾掉了。

「外面那些傢伙還在耶。它們平常明明就會逃走。」

「難不成……目前為止都只是偵查？」

「也就是說……有騎士以上的高階種！」

哥布林是弱小的魔物，但智力算是很高。

事實上，它們為了成群狩獵會分成好幾個群集偵查，發現獵物就會和伙伴報告，並擬定先下手為強之類的戰略，直到獵物變得衰弱為止，都會進行糾纏不休緊咬不放的戰鬥方式。

就動物上來說，它們的習性應該很接近鬣狗。會透過製作、使用武器來減少損耗並提升攻擊力，所以對村民而言，意外地會是很棘手的對手。

實際上，圍住這座村子的牆壁是高牆的構造，內牆與外牆之間有可以通行的道路，但那裡卻被開了個洞，讓外敵得以入侵。

恐怕是反覆襲擊了好幾次，而調查過內部構造了吧。

剛才襲擊而來的，是為了入侵而設下的誘餌。主力的哥布林則是躲了起來，一點一點不停對牆壁開洞，如此便能成功侵入村莊。雖然說是魔物，也絕不能小瞧它們的智力。

「爬上外牆！用弓迎擊！」

「這些傢伙是怎樣啊……原來有這麼多嗎！」

「好啦，趕快！別讓它們入侵村子半步！」

「這場戰爭結束之後，我就要和女朋友結婚了……」

「真好啊。那麼，戰爭結束後到酒館喝一杯吧。我請你喝上好的酒。」

「你也要準備沙拉喔。有加鳳梨的那種。」

雖然也有人在立死亡旗，但澄香還是趕緊爬上了包覆在外圍的外牆上，試著看看外側的狀況。

哥布林的數量應該隨便就超過了一百。它們散了開來，盯准村子防御最薄弱的地方攻過來。

如剛才敘述過的，哥布林絕對不是愚蠢魔物，它們擁有在大自然生存時理解狀況、盯准敵人弱點，並確實攻過來的智力。

遊戲裡是三兩下就會被打敗的嘍囉角色，現實中卻是最狡猾且勤勉的生物。知道要以數量彌補自己的弱小。

它們很可能襲擊了村莊好幾回，並推測了對手的戰力。既然集體攻了過來，就表示有準備好確保侵入路線的大略基準，但澄香沒有了解到那種程度。

那也是攸關生存的戰鬥之一。

「首先，必須瞄準聚在一塊的部分呢。『蓋亞之矛』。」

因為澄香使用的魔法，地面突然冒出無數岩槍，將哥布林從腳邊往上串起刺死。她緊接著使用「岩針領域」，讓哥布林無法輕易靠近。

哥布林本來就不可能穿什麼鞋子，無法前進充滿荊棘的地面。

面對在這種情況下驚慌失措的哥布林，村人們放箭應戰。

「無咏唱啊？你年紀輕輕，卻很厲害耶。」

「現在不是說那種事情的時候吧！要繼續上嘍！」

「好！接著小姑娘攻擊下去！把哥布林們全宰了！」

「「「「「喔喔喔喔喔喔喔喔喔喔喔喔喔喔喔喔喔！」」」」」

村民們順勢一鼓作氣地果斷用弓箭一再攻擊。

然而，雖然弓箭確實很有利，但要一擊打倒哥布林，就只能瞄準頭部。不過，村民的本領沒那麼高強，他們無法瞄準，只是在隨便放箭。

不過，這在牽制上也很有效果，哥布林無法貿然進攻。

「好機會！『龍卷風』。」

形成的龍卷風像在捲入哥布林們那樣輕鬆吞噬了群集，從那兒被拋出的哥布林則摔到「岩針領域」上。

被無數的尖刺刺中，哥布林痛苦得四處打滾。

「……小姑娘，你好狠啊……真是無情。」

「…………………………」

雖然是偶然的結果，但那幅光景太殘酷了。

它們簡直像在針山受審的罪人，是幅淒慘哀號的地獄圖。

「比、比起那種事，其他哥布林呢？」

「正在攻入西側！誰快輪派去那邊吧！」

「大叔，這裡就拜託你。我去！」

「小心喔。尤其流箭很可怕。」

「謝謝。我沒問題，這裡就麻煩嘍！」

澄香跑在防御牆上，哥布林好像打算分成數個部隊襲擊，但人數最多的部隊，已經因為澄香之手而處於毀滅狀態。

然而，她沒看見關鍵的高階種的踪影。哥布林本身沒什麼了不起，但數量太多也很令人傷腦筋。它們還在外牆之外，所以算是沒關係。要是被入侵就會變得很棘手了吧。

澄香不斷奔馳，留意到群聚的哥布林正貼著牆壁，打算攀登的光景。她立刻發動魔法。

「『爆破』。」

──轟隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆！

炎系最強魔法──「爆破」。

這個異世界裡，可以使用這項魔法的魔導士，說實在應該相當少吧。

由於它是大範圍魔法，故威力是最高級別。甚至受到熱浪餘波襲擊的哥布林們都被燙傷。

她盡量集中以控制損害範圍，但他們聚在一塊，所以損害相當大，簡直就像熾熱的地獄。全身著火四處滾的哥布林顯得很可怜。

「呵、呵、呵，投入所有的財產買下魔法卷軸是正確的呢……啊，魔力不夠了，糟糕……會暈眩……」

這個魔法的缺點，就是因為它是未改造的魔法，所以耗魔量很凶。如果亂射的話，魔力馬上就會枯竭。不過，她也沒有可以亂射的魔力吧。

澄香急忙喝下魔力藥水「能量補充．Great！」讓魔力恢復。她總覺得在此脫離戰線會很不妙。

這個回復道具，是她走在遊戲的城鎮裡，跟在街角販售道具的「可疑小販」那裡買來的東西。效果非常出色。

順帶一提，遊戲時，既有的未改良魔法一般在魔法店就可以購買。

「好強……她是哪家的孩子啊？」

「不是我們村子的吧。是傭兵嗎？是說，她還真厲害……」

「嗯……哥布林就像塵埃一樣……」

哥布林因為剛才的一擊，幾乎都被驅散掉，現在即使是村人們似乎也足以應戰了。

「欸，沒有其他哥布林了嗎？」

「這裡沒有。不過，沒看見那傢伙的踪影呢。」

「那傢伙？啊！是哥布林騎士，對吧？」

「它平時都會在那些傢伙當中，現在卻不見踪影，是消失去哪兒了？」

即使是哥布林或其他魔物，它們在率領群集上都是其中最強的魔物。

不見該魔物的身影是很奇怪的。明明這麼大規模攻過來，領袖魔物不可能不現身。它很可能是在某處觀察戰況，或是在哪裡設置某些圈套吧。

她想起VRRPG的哥布林也會像那樣制定作戰攻打過來。

「叔叔們，有沒有哪裡人手比較少呢？雖然是大概，但外面的傢伙可能是佯攻喔。」

「這種數量嗎！可是……它們可是哥布林耶。我不覺得腦筋有好到這種程度。」

「也是呢，畢竟是哥布林……」

普通村民沒有魔物的知識，認為哥布林也只有和野生野獸同等的智力。

這就是一般人的認知，不認為它們會像人類一樣制定作戰攻過來，就是這世上的常識。但，那份認知卻是錯誤的。

正因為是弱者，才會思考如何有效率地打倒敵人，人類也是那樣存活過來的種族。怎麼能說哥布林就不一樣呢。

「農田有敵襲！那傢伙出現啦────！」

所有人望向農田，就看見剛才澄香堵住的洞穴冰塊被弄碎，出現了特別大只的哥布林。

不過，那不是哥布林騎士，而是它的高階種。

「是哥布林將軍……那可不是騎士喲。」

「「「「什麼──────────！」」」」

哥布林將軍，是僅次於哥布林國王或皇后的強大魔物。事到如今，它擁有讓人不認為是哥布林的強度，憑一般的傭兵是應付不來的。

看來它好像在最初潛入的部隊裡，但因為澄香的魔法堵住了洞穴，而無法從防御牆的內牆與外牆之間出來。

哥布林將軍和其他哥布林一起撲向了村人們。

「逃啊！是哥布林將軍！要是應戰可會死人！」

「可惡！外面還有哥布林！」

「現在先逃並關上農田的門吧！我們無法當這些傢伙的對手！」

它並非所向無敵，但如果是高階種，魔物的強度就會躍升，因此也相對能提升那麼多危險度。

村民要當它的對手，再怎麼說都有點困難。

「我不擅長肉搏戰耶～」

澄香專職魔導士，但並非無法進行肉搏戰。

她無疑遠比村人的戰力還來得強。她馬上就下來農田，揮舞「盧恩木杖」，把一副囂張在田裡四處走動的哥布林打趴。

女性雙人組的傭兵好像也在應戰哥布林，無奈數量很多。

她們不可能應付所有哥布林，外面的哥布林也同時入侵而來，這樣下去村子應該會淪陷。

重要的是有哥布林將軍在。那位哥布林將軍猛然逼近紅髮女性。

「嘉內！」

「什麼？咿！」

雖然她好不容易接下了哥布林將軍的劍擊，但那一擊很沉，紅髮女性被掃飛了出去。同行女子則在動搖之際遭到攻擊，雖然勉強避了開來，這次卻是被哥布林們給團團包圍。

「『歸向導引・閃電之箭』！」

不知被輸入多少電力、具有追尾性能的雷箭，讓哥布林們一瞬間死亡。觸電的哥布林身體麻痺，叫嘉內的紅髮女性就趁機用巨劍擊飛了哥布林的首級。血液噴了出來，把她染得一身紅。

「唔……好惡。」

「真是幫了大忙。我是嘉內，另一位是雷娜，我們是被這個村子委託僱用的傭兵。不過，那傢伙很棘手喔。」

「我叫……伊莉絲，流浪的魔導士。哦～那麼我會用輔助魔法強化，兩位就重複進行打帶跑吧。」

她不小心以遊戲感覺自報姓名，但澄香決定別在意這種事，心想「唉，算了」。

現在的狀況不能因為名字的事分心。

「我知道了。但，它比我所想的還頑強喲。」

「唉，因為是哥布林將軍呢……是說，它來了！」

不知是因為伙伴被殺而火大，或是純粹的自暴自棄，哥布林將軍一邊胡亂揮劍，一邊往這裡猛衝過來。

「散開！」

三人配合嘉內的聲音，各自散開。

「『增強力量』、『力場護盾』、『速度增幅』。」

「發動多重魔法？年紀輕輕，真難以置信！」

「不管怎麼樣，都幫了大忙。喝──────！」

嘉內砍上去，哥布林將軍就想擋住攻擊並且橫砍，但她很快就跳到後方，以順著哥布林將軍斬擊的形式，離開了它的攻擊範圍。

「雷娜！」

「好的好的，交給我！」

叫做雷娜的女性打帶跑之後，馬上就縮短對峙距離，從背後發動攻擊，接著再次離開敵人身邊。澄香在那空檔發動魔法牽制。因為無法在田裡使用炎系魔法，所以她就使用地系魔法「岩彈」加以攻擊。

嘉內和雷娜則會在那個空檔攻擊，透過澄香進行掩護，來玩弄高階種。

『咦？這只哥布林將軍……很弱耶。』

網路遊戲出現的哥布林將軍，雖然也是有等級差距，但那算是很不好對付的怪物，澄香有種覺得不足夠的感覺。

「唉，算了。飛走吧，『電漿重擊』！」

一道極寬的閃電從哥布林將軍頭上劈落，雷娜在它變得焦黑的瞬間，從它身後將廓爾喀彎刀刺了進去，把將軍從頸部底端斜砍至腋下。

這裡不同於遊戲，只要連細微的破綻都不放過並予以一擊，即便敵人是高階種也是可以打倒的。

剛才，哥布林將軍因為澄香的攻擊身體麻痺，因此束手無策。

這果然和遊戲不同。現實中的哥布林屍體不會消失不見，而會被留在原地。

村子外面好像也結束了戰鬥，村民們從圍住四周的防御牆下來。

「好厲害，你們打敗將軍了啊！」

「叔叔，外面的哥布林怎麼樣了呢？」

「全都逃走了。我們暫時是安穩了呢。」

「哥布林本身很弱，但湊齊數量就會很棘手呢。」

「說得沒錯。這樣就暫時可以放心睡了。那麼，再多做一件工作吧……」

村子的男人們拿刀插入哥布林，剖開了哥布林的腹部，接著拖出了內臟。澄香因為這幅太過殘酷的光景，而差點昏厥過去。

然而，這是為了支解並確保魔石等稀少素材的重要行為。

說起來，哥布林是被稱作「沒有可用素材」的沒用魔物，若能確保魔石就值得慶幸了。

即使賣了那些魔石也沒几塊錢，依然或多或少是筆收入。

「嘖，這裡沒有魔石。是年輕的傢伙呢。」

「我這裡算是有魔石，但很小顆……這個能賣嗎？」

「我支解的那只算是很大顆喔。你們運氣很不好呢。」

雀躍地支解哥布林的光景有點恐怖。男人們邊笑邊支解人形生物，一片相當異樣的光景映入了澄香的眼簾。

兩位女性傭兵好像也在支解哥布林將軍，澄香不小心看見個某出慘劇舞台。

「小姑娘，支解結束之後，你就把屍體燒了吧。畢竟幾乎都是你打倒的呢。」

「嗯……嗯，我有點累了，所以正在休息……」

「哦，因為是魔導士呢。你連續發了那麼多魔法，應該也會發生魔力耗盡的情況吧。」

澄香並不是魔力見底。她還留有大約一半的魔力，看起來也沒有特別疲累。她只是看見支解的情景，變得不舒服而已。

後來，他們燒了被集中到一處的哥布林屍體，但哥布林的屍體以高溫燃燒的話，就會產生異樣的臭味。

那真的是到了催人嘔吐的程度……

三十分鐘後。

「欸，伊莉絲……你剛才說『流浪』，為什麼你要獨自旅行呢？」

「因為我想逃脫無趣的日常生活……我想進行刺激的冒險……嘔嘔嘔～！」

「沒辦法『支解』是要怎麼辦？那可是傭兵的必要技能喲。」

「我專職魔法……會在這方面努力，所以沒關係……支解我沒辦法。」

一直以來生活在現代社會中的澄香，根本沒辦法支解到剛才都還活著的生物。

不過，現在的她也不可能會知道，這場邂逅會使得她們三個人開始組隊行動。選項不知不覺間就顯現了出來。

後來，這個村子完成工作後，舉辦了一場簡單的宴會。

◇　◇　◇　◇　◇　◇　◇　◇

第二天依然持續處理哥布林屍體。村子現在仍處於祭典般的狂歡之中。

也因為長期受哥布林折磨，害獸消失使得城鎮恢復頻繁的往來，澄香終於可以去城裡了。

雖然只有一點點，但她得到了旅費，也可以寄宿好幾天。

「哎呀～小姑娘來村子真是幫了大忙耶。想不到會出現將軍。你真了不起！」

「那些話，我昨天也聽過嘍。比起那件事，我也收到了錢，這樣好嗎？」

「你沒辦法支解吧？比起你救了村子應得的回禮，這只是份薄禮啦。」

不是所有哥布林身上都可以獲得魔石，即使如此也有一筆相應的數量。多虧在村子裡唯一的雜貨鋪把所有魔石全數拿出去換錢，澄香的手頭也寬裕了一些。

「小姑娘，你接下來要怎麼辦？」

「嗯～和嘉內小姐她們去城鎮之後登錄成傭兵，並且累積實力。我也想去去地下迷宮之類的地方呢～」

「你想要一舉致富嗎？賭運氣也該有個限度喔，那可是攸關性命。」

「與其說是錢財，應該是冒險吧？我想看看至今不曾見過的世界。」

「冒險嗎～……我年輕時也曾經夢想過。」

澄香明天就會離開這座村莊，前往城鎮。她打算在那裡登錄成傭兵，自由地四處看看嶄新的世界。

「你要去桑特魯城嗎？要小心喔，那裡也有很多不正經的大人。」

「謝謝，我會試著努力的。」

雖然是幾天期間，她和村民們也做了交流，算是變得很親近。

要離開這個村子也總覺得有點寂寞，但那是澄香第一次自己選擇的路。

「伊莉絲，你知道雷娜那傢伙在哪裡嗎？我在任何地方都沒看見她……」

「咦？沒看見……雷娜小姐不在嗎？」

「嗯……她該不會是出手了吧？」

「你是指什麼？」

澄香歪頭，就看見那位雷娜從旅館出來的樣子。

皮膚光潤……

「她剛才從旅館出來……皮膚非常光滑呢。」

「果然啊……」

澄香覺得不可思議地俯瞰突然抱住頭的嘉內，同時歪了歪頭。

「什麼果然呀，嘉內？」

「雷娜……你又做了對吧？」

「嗯……做了。很可愛喲～♡」

此時的澄香，不知道雷娜是去幹了什麼事。

知道她的性癖，則是不久之後的事了。

「諸位！開宴會啦！開大型宴會啦啊啊啊啊啊啊啊啊啊！」

「長老興致真是高昂。」

「你血壓又會上升然後昏倒喔。」

就這樣，他們今天也開了宴會，據說喧鬧持續到了深夜為止。

隔天，澄香和宿醉的嘉內等人一起搭上馬車，前往據點城鎮桑特魯。那是為期約五天的馬車之旅。

澄香把自己的名字換成伊莉絲，向新世界的生活邁出一步。

她小小的胸口懷抱著夢想與希望，馬車載著這樣的她，徐徐前往城鎮。

伊莉絲的異世界生活，於是就這麼展開了。

一個半月後，伊莉絲再次完成了新的邂逅。

對方是稱霸最強之座的五位魔導士之一，是她也很憧憬的「殲滅者」。

此時的她，仍無從得知自己將和大叔魔導士長久來往下去。