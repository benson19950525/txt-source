然後，我們抵達了障壁塔。
除了沒有戰鬥力的伊緹以外的四人都下了馬車。

「那麼伊緹就和金屬龍一起在外面待機。金屬龍因為大小也沒辦法進到障壁塔裡面。」
「收到，還請注意自身安全，艾伊西亞大人。」

趁著艾伊西亞和伊緹說話的空隙，我按下藏在胸部谷間遙控器的開關。

「（那麼金屬龍二世、金屬龍三世，接下來就拜託嘍）」
『Yes，Ma'am』『OK』

瞬間，頭上傳來機械音效的咆哮。
接著出現的是，今天早上收到我的命令就一直在障壁塔頂端待命的金屬龍三世。為了這一天，我還特意給金屬龍三世加了角以及棘刺等，讓牠看起來更加邪惡。

「漬！各位，從上面來了！快趴下！」
「什⋯⋯那個是，巨大的金屬龍！？」

我以迫真的演技跟著大家一起趴下。

瞄準上空飛來的金屬龍三世，地上的二世放出光束砲。

「Feuer」

然而因為這也是事先說好的光束砲，所以三世輕輕鬆鬆就避開了。
然後，二世眼睛的燈光從紅轉藍。

「System，AerialMode」

伴隨並不是必要的變形，二世的背上跑出一對翅膀。

「你這東西⋯⋯你這東西才沒這個機能吧！？」

雖然安古這樣大叫，但二世無視掉就這樣起飛。
二世和三世在空中激戰，二世因為相差一倍的體格差處於弱勢。但因為二世順利地架開對手的攻勢，三世就這樣被打落到地面。到這邊都和事前說好的一模一樣。

「哈！」

知道三世落下地點的我，比誰都早地砍向三世。
從上段砍下的一刀，切落了沒有用的裝飾機械龍角。金屬龍三世發出動苦的悲鳴。理所當然的，三世實際上並沒有受到任何傷害。

「AAAAa」
「──！」
「影耶！」

金屬龍三世（裝作）因為疼痛而揮下巨腕。按照劇本擺出格擋姿勢的我，也自己用力踢向地面，裝作被用力擊飛到了障壁塔的入口。

雖然我漂亮地兩腳著地，但從鞋子放出的衝擊波發出了「被狠狠吹飛撞上牆壁」的聲音。

好了，接下來⋯⋯⋯

我從胸部的谷間取出遙控，與金屬龍們進行通信。

「⋯⋯做的非常完美，二世、三世」
『Year』『Thankyou』
「你們之後就繼續在空中纏鬥。《瞬間換衣》《初始化》」

換上假面和大衣，在變成原本的身姿就成為了地獄的鍛造師了。

好啦，現在開始就是正戲了。一起狂歡吧。

※

因為突然出現的巨大金屬龍，影耶被吹飛到障壁塔之中。

被影耶切落機械龍角的巨大金屬龍雖然在地面暴動了一會兒，但馬上就又飛回到空中。

「Feuer！」「Feuer！」

雙方的金屬龍各自放出光束砲，產生巨大的爆炸。

「沒想到鍛造師伊亞製作的格雷姆竟然會有如此力量⋯⋯！」
「伊緹！你趕快先逃！」
「是、是的！」

伊緹因為艾伊西亞的命令先行離開。

「光彌！安古！我們也趕快進去障壁塔之中！只要捉住施術者，應該也就能停下那個大只的金屬龍了！」
「啊啊！」
「明白！」

跟隨著影耶的腳步，我們也突入了障壁塔之中。
障壁塔的內部和外壁相同，都是由黑鋼一般的材質打造而成的。
內部挑空的設計直達最高層天花板，然而在染上一片的漆黑的障壁塔中，這仿彿就是漆黑無光的夜空。

然後，在這深黑的塔中，有個男人像是融入房間般的站在中間。

「黑色假面和大衣，以及藍色的頭髮⋯⋯！」
「哈哈哈，歡迎來到我的壁壘，勇者一行人唷！沒錯，我就是從地獄爬出的鋼鐵操縱者！至高的鍛造師伊亞！這個鍛造塔將會是你的墳墓！哈哈哈！」

是腦袋不正常嗎，伊亞情緒異常高昂地不停高笑。

我環顧四周，卻都沒能發現影耶的身影。

「喂，伊亞！你把影耶怎麼了！」
「哈哈哈，別著急啊，勇者。影耶的話，現在應該正和我所製造的格雷姆們戰鬥中。真是的，強到令人煩躁的傢伙」

像是看好時機般，從牆壁後方傳來了戰鬥的聲響。應該是影耶戰鬥造成的聲音吧。

「好了，雖然初次見面就這樣也很不好意思，但這就是最後了。──交出聖劍。因為我才有與之相應。」
「⋯⋯我拒絶！」
「哼，你想說只有你才能打倒魔王嗎？戰鬥可不是一個人的力量就能左右的。只有像我一樣擁有絶對軍事力量的人，才配稱為絶對唯一的勝者！」
「⋯⋯你的意思你要打倒魔王？為了什麼？」
「欸？這個⋯⋯⋯⋯⋯⋯哼哼哼，不明白嗎？姑且也是身為勇者之人，竟然連這麼簡單的事情都不知道！哈哈哈哈哈哈！」

伊亞這樣說著大笑，但做出這種事情的人在想什麼我怎麼可能明白。
這個時候，艾伊西亞擺出發現什麼事情般的表情並開口說道。

「難道⋯⋯⋯難道你是想說你要自己化身為魔王嗎！？」
「！⋯⋯沒錯，就是這樣！哈哈哈哈，真不愧名為遐邇的巫女姫！和空有稱號的勇者完全不一樣！但是，那個⋯⋯我對毀滅人類沒有興趣。我的目的是將我的霸業刻上歷史的一頁，只要這樣就行了！」
「誰要你這種人的稱讚！還有，要打倒魔王的人不是你，是我的光彌！」

艾伊西亞這樣說完，發動了魔法。
她的周圍展開了以同心圓展開數個環形魔法陣，光的粒子隨之飄舞。

「《獅子座的大鐮》！」

艾伊西亞用魔法編織而出的巨大光之鐮刀，跟著她手腕的動作，宛如沒有重量地砍向伊亞。

伊亞腳邊瞬間閃過紫電，瞬間──鐮刀被從地板冒出的柱子給擋住了。⋯⋯不對，考慮到周圍地板凹下去的樣子，說不定是讓地板變形了。

「什麼！？」
「《瞬間改造》⋯⋯⋯真是的，還真是千鈞一髮。舞台明明才剛開始而已⋯⋯」

伊亞疲憊地聳肩。

「嘛，也行。現身吧，吾之鋼鐵軍勢！無視其他傢伙，直取勇者首級！」

伊亞如此下令的瞬間，許多巨物從天花板一落而下。
兩公尺高的鋼鐵格雷姆們，阻擋在伊亞和我們之間。

格雷姆以無法想像是鋼鐵打造的速度，執拗地只狙擊我一人。

「光彌！《固岩之壁》！」

安古發動魔法生成岩壁，防下了格雷姆的拳頭。
然而敵人並不只一體。繞過岩壁的格雷姆打出的一擊，我連忙用聖劍接下。

「咕！」
「光彌，用神聖魔法！」
「對喔！《褉之波動》！」

從我身體放出的閃耀光輝開始淨化伊亞放出的格雷姆。

──然而，格雷姆卻沒有停止動作。雖然一瞬間鈍了一下，但馬上就又照常襲擊過來。

「怎麼會！？為什麼神聖魔法會沒有效⋯⋯」
「這座障壁塔下的所有魔法道具，全都由本大爺我不間斷地供應魔力。只中斷一瞬間的魔力是沒有任何意義的！」
「也就是說，只要打倒你就萬事解決了吧！《風之連弩》！」

安古兩手放出魔法陣，並從中放出風屬性的魔法之矢。
記取剛才艾伊西亞的魔法被防御下來的教訓，所有的魔法箭都不停改變著軌道。

然而，這也全都被伊亞身邊的一把浮游劍全部砍落。

「怎麼可能！？」
「『杜蘭達爾・自動戰鬥型態』。⋯⋯真是優秀的判斷力和魔法控制技術，安古。看來有必要修改一下對你的評價。」
「恩？你這傢伙，知道我的事情嗎⋯⋯？」
「咳咳咳！好了，吾之格雷姆唷，速速擊潰勇者！」

格雷姆們聯手展開比剛才更強的攻勢襲向光彌。安古使出各式各樣的魔法想要打破伊亞的防御，但因為浮游劍所以都無法如願。

「咕⋯⋯！」

現在被作為勇者強化過的我，只要蓄積力量鐵塊等級也是可以斬斷的。然而對手太多實在沒有蓄積力量的空閑。
等等，如果一個一個打倒不行的話──

「艾伊西亞！拜託了，幫我爭取一點時間！」
「我知道了！交給我！《獅子座的大鐮》！」

艾伊西亞的光之大鐮掃開格雷姆們。雖然無法破壊格雷姆，但衝擊也使它們退了數步。

趁著那個空隙，我把聖劍收回劍鞘。然後一邊回想昨天所見到的光景，一邊往劍鞘蓄積魔力。
白光如同火花噴散般從劍鞘溢出，劍鞘中儲存的魔力化為聖劍拔刀時的壓力，也將化作吹飛敵人的刀身。

「⋯⋯！你這傢伙，該不會──」

我在伊亞轉過來看向我的瞬間，解放了魔力和聖劍。

「《雙聖一刀・白龍劍》！」
「不要隨便抄襲別人的技能呀！」

伊亞叫喚著什麼，但那聲音也被純白的一擊所產生的爆音給埋沒。

光芒消失後，留下的只有化作鐵片殘骸的格雷姆們。

「該死的傢伙！魔法砲台，給我把勇者射成蜂窩！」

因為格雷姆被破壊而沒有餘裕的伊亞這樣大聲叫喊。
牆壁突然打開好幾個洞，並彈出許多類似機械弓的東西。

然後像是機械弓的東西開始拉緊閃爍各色光芒的魔法之箭的箭弦。

「射擊！」
「休想得逞！《星之結界──射手座的智慧》」

艾伊西亞站到我的前面召喚出環形魔法陣，並使數個閃耀的光球浮游於周圍。
發射出來的魔法箭在接近到光球的瞬間，便反轉軌道射回機械弓的方向。

被反轉軌道的魔法箭射中的機械弓一一爆炸，最終全部停止了機能。

「謝謝你，艾伊西亞」
「哼哼，這點小事輕而易舉！」
「接著就只剩下你了，伊亞！」

※

勇者隊伍比想像中的還強啊。你們是怎樣啦全隊都一起作弊嗎。魔王等人也真辛苦，竟然得應付這些傢伙。
不過現在老實說是有點不妙。沒想到竟然會被無傷撐過這波。

我的預想中，勇者隊伍就算稱過了也應該多少要受到一點傷害才對⋯⋯⋯
嘛，算了。這樣也就只是換到計劃Ｂ而已。不過照現在這樣，計劃Ｂ感覺也會被撐過去⋯⋯⋯

「⋯⋯哈哈哈，你們以為這樣就結束了嗎？天真，太天真了！哈哈哈哈哈！啊哈哈哈哈哈哈哈哈！」
「有什麼好笑的！」

不笑不行啊你個該死的傢伙。
我一邊自暴自棄地高笑，一邊翻了一下大衣。

這個瞬間，一直被封閉在大衣之中，纏繞著不詳魔力的風吹了出來。另外，這單純只是特效，就算碰到也不會有任何事情。
然後以這為信號，地面也跟著開始搖晃。

「什麼！？這個地震是！？」
「呼哈哈，感到害怕了嗎？」

你們肯定想也想不到這是金屬龍們在外面蹦蹦跳跳製造出來的吧。

「哈哈哈哈！就讓你們看看吧！這就是我的最強的一招！我至高的究極奧義！其名為──！」

因為沒想到名字，所以我就按下了袖口中的開關。
然後按下去的同時，事先放在二樓的阿佐特發出了黑色的光線魔法《黑魔的咆哮》。對著我。

「咕啊啊啊啊！這、這個魔法是！？是你嗎，影耶啊啊啊！！」

我半認真地發出悲鳴。《黑魔的咆哮》雖然是沒什麼攻擊力的魔法，但對身體是一般人的我來說還是很痛。

「（《自我改造》！《瞬間換衣》！）」

沐浴在黑色光芒之中，我使用改造魔法變身成為影耶。雖然使用改造魔法會發出強烈的光線，但如果是現在這個光線之中就沒關係了。

光線消失後，出現的是變成影耶身姿的我。
我剛才的光線也受了點傷，所以裝作「直到剛才都還在別的地方戰鬥」也是毫無破綻。但換到淚腺脆弱的影耶已經痛到讓我想哭了。好痛。
忍著淚水，我對著光彌等人說明事情的由來（劇本）

「咕⋯⋯，被伊亞那傢伙逃了嗎⋯⋯」
「影、影耶！？你沒事吧！？」
「啊啊⋯⋯⋯沒事的，唔、這只是小擦傷而已。唔⋯⋯」
「可是我看你好像很疼的樣子。」
「只是擦傷。剛才我從二樓放出魔法並跳了下來，但伊亞反過來利用了我的魔法，在魔法之中強行突破逃往了上層。我想應該是向著最上層去了吧」
「那麼我們也──」
「沒戲的，你看」

在光線停下的同時，本來挑空的天花板也跟著關閉。當然這也是事先設計好的。

「也就只能一層一層地爬上去了」
「原來如此⋯⋯⋯不過，在這之前⋯⋯」
「還有什──」

下個瞬間，我的眼前一片漆黑。

這什麼，新的魔法嗎？

「你平安無事，真的是太好了⋯⋯」

從上方傳來了光彌的聲音。
難道，難道說，我被光彌緊緊抱住了嗎？

「我──」
「放開我笨蛋！」
「咕噗！？」

我腳蹬了一下，用頭錘撞向光彌的下顎。
別隨隨便便碰我可愛的影耶！殺掉你唷！不對本來就要殺掉了。

「什麼都還沒有結束！什麼沒事真好，趕快往上走了！」
「唔⋯⋯等、等等⋯⋯」
「沒事吧光彌！？喂影耶，你給我停一下！」

我無視光彌走向樓梯那邊。
安古從旁邊跟了上來，並戰戰兢兢地向我搭話。

「呃，那個，影耶小姐，你的臉有點紅⋯⋯」
「啊啊，是呢」

因為憤怒。

然而，已經不能再說喪氣話了。
我下定決心，不管怎樣都一定要在接下來的計劃Ｂ確實地打倒光彌。